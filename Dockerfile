FROM docker:latest

RUN apk add py3-pip py3-ruamel.yaml py3-docker-py py3-chardet py3-entrypoints py3-jinja2 py3-json-logger py3-requests py3-semver py3-toml py3-traitlets py3-iso8601
RUN pip install --break-system-packages "jupyter-repo2docker==2023.6.0"
